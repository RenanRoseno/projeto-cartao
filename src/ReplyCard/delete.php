<?php
namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');


$confirm = 0;
?>
<!DOCTYPE html>
<html>
<head>
	<title> Gabaritos</title>

	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/style.css">
	<link rel="stylesheet"  type="text/css" href="../../frameworks/iziToast/css/iziToast.min.css">
	<script type="text/javascript" src="../../frameworks/iziToast/js/iziToast.min.js" ></script>
</head>
<body>

	<script type="text/javascript">
		function exibir(){
			iziToast.show({
				theme: 'dark',
				icon: 'icon-person',
				message: 'Excluido com sucesso!',
				position: 'topCenter',
				progressBarColor: 'rgb(0, 255, 184)',
				buttons: [
				['<button>OK</button>', function (instance, toast) {
					window.location.href = "../index.php";
				}]],
				onOpening: function(instance, toast){

					console.info('callback abriu!');
				},
				onClosing: function(instance, toast, closedBy){
					window.location.href = "../index.php";
				}
			});
		}

		function erro(){
			iziToast.show({
				theme: 'dark',
				icon: 'icon-person',
				message: 'Erro ao excluir!',
				position: 'topCenter',
				progressBarColor: 'rgb(255, 50, 0)',
				buttons: [
				['<button>OK</button>', function (instance, toast) {
					window.location.href = "../index.php";
				}]],
				onOpening: function(instance, toast){

					console.info('callback abriu!');
				},
				onClosing: function(instance, toast, closedBy){
					window.location.href = "../index.php";
				}
			});
		}

	</script>

	
	<?php
	$id_gabarito = base64_decode($_GET['id_gabarito']);

	$query = "DELETE FROM gabarito_questoes WHERE id_gabarito = $id_gabarito";
	$result = pg_query($connection, $query);
	$query = "DELETE FROM gabarito_alunos WHERE id_gabarito = $id_gabarito";
	$result = pg_query($connection, $query);
	
	$query = "DELETE FROM gabarito WHERE id = $id_gabarito";
	$result = pg_query($connection, $query);
	
	if($result){
		echo ("<script type='text/javascript'>
			exibir();
			</script>");
	}else{
			echo ("<script type='text/javascript'>
			erro();
			</script>");
	}
	?>

</body>
</html>