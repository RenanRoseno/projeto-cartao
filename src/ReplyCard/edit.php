<?php
namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');

$id_gabarito = base64_decode($_GET['id_gabarito']);


$query = "SELECT tipo, gabarito.id, questoes, professor_id, data, qtd_questoes,qtd_itens 
FROM gabarito_questoes, gabarito, tipo_gabarito 
WHERE id_gabarito = gabarito.id AND
id_gabarito = $id_gabarito AND  
id_tipo = tipo_gabarito.id;";

$result = pg_query($connection, $query);

$row = pg_fetch_array($result);
$qtd_questoes = $row['qtd_questoes'];
$qtd_itens = $row['qtd_itens'];

$itens = ($qtd_itens == 4) ? array( 0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D') : array( 0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E');

$questoes = json_decode($row['questoes']);
?>
<!DOCTYPE html>
<html>
<head>
	<title> Gabaritos</title>
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<center><h2> Editar Cartão-Resposta</h2>
	<form action="update.php" method="POST">
		<input type = "hidden" name="id_gabarito" value='<?php echo $id_gabarito ?>'>
		<input type = "hidden" name="qtd_questoes" value='<?php echo $qtd_questoes ?>'>
		<?php


		foreach ($questoes->targets as $key => $questoes) {
	//echo $questoes->id;
			$questao = $questoes->id."-".$questoes->item;
			
			echo "Questão ".$questoes->id.": ";
			echo "<div class='form-check form-check-inline'>";
			for($i = 0; $i < $qtd_itens; $i++){
				$checked = ($questao == $questoes->id."-".$itens[$i])? "checked" : "";
				echo "<input type='radio' class='form-ckeck-input' id='$questoes->id' name='$questoes->id' value='$itens[$i]' $checked>";
				echo "<label class='form-check-label' for='$questoes->id'>$itens[$i]</label>&nbsp;";

			}
			echo "</div><br>";
		}
		?>
		<br>
		<input type='submit' class='btn btn-success' value='Atualizar'>
		<a class='btn btn-danger' role='button' href='../index.php'>Cancelar</a>
	</form>
</body>
</html>




