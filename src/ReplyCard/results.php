<?php
namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');

$id_gabarito = base64_decode($_GET['id_gabarito']);

$query = "SELECT nome,gabarito_alunos.id as ida, tipo, gabarito_alunos.id, id_aluno, nota, acertos, erros, id_gabarito, respostas, gabarito_alunos.data
FROM public.gabarito_alunos,tipo_gabarito, gabarito, g_alunos
WHERE id_gabarito = $id_gabarito 
AND gabarito_alunos.id_gabarito = gabarito.id 
AND g_alunos.id = id_aluno
AND id_tipo = tipo_gabarito.id ORDER BY acertos ASC;";

$resul = pg_query($connection, $query);
$result = pg_fetch_array($resul);

$res = pg_query($connection, $query);
?>

<html>
<head>
	<title> Gabaritos</title>
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<center>
		<div class="container">
			<h4>Resultados</h4>
			<h6><?php echo $result['tipo'];?></h6>
			<h6><?php echo substr($result['data'], 8, -15)."/".substr($result['data'], 5, -18)."/".substr($result['data'], 0, 4);?></h6>
			<div class="row">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th scope="col">#</th>
							<th scope="col">ID-Aluno</th>
							<th scope="col">Nome</th>
							<th scope="col">Acertos</th>
							<th scope="col">Erros</th>
							<th scope="col">Nota</th>
							<th scope="col">Ações</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 0;
						while ($row = pg_fetch_array($res)) {
							echo "<tr>";
							echo "<td>".++$i."</td>";
							echo "<td>".$row['id_aluno']."</td>";
							echo "<td>".$row['nome']."</td>";
							echo "<td>".$row['acertos']."</td>";
							echo "<td>".$row['erros']."</td>";
							echo "<td>".$row['nota']."</td>";

							$id = $row['ida'];
							echo "<td>".
							"<a class='btn btn-info btn-sm' role='button' href='showGabarito_a.php?id_A=$id'>Mostrar</a></td>";
							echo "</tr>";
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
	</html>
