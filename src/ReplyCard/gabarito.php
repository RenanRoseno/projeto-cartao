<?php
namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');


$qtd = $_POST['qtd'];
$materia = $_POST['materia'];
$itens = $_POST['itens'];
$elements = [];


?>
<html>
<head>
	<title> Gabaritos</title>
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<center>
		<div class="container">
			<h4>Cartão Resposta</h4>

			<div class="row col-md-2">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th scope="col">Questão</th>
							<th scope="col">Item</th>
						</tr>
					</thead>
					<tbody>
						<?php
						for($i = 1; $i <= $qtd; $i++){
							echo "<tr>";
							if($i < 10)
								$questao = "0".$i;
							else
								$questao = $i;

							$item = $_POST[$questao];
							echo "<td>$questao</td><td>$item</td>";

							$temp = array('id'=> $questao , 'item' => $item);

							array_push($elements, $temp);
							echo "</tr>";
						}

						$targets = array('targets' => $elements);

						$targetsJSON = json_encode($targets);
						?>
					</tbody>
				</table>
</center>
				<form action="inserir.php" method="POST">
					<?php 
					echo "
					<input type='hidden' name='itens' value='$itens'>
					<input type='hidden' name='qtd' value='$qtd'>
					<input type='hidden' name='materia' value='$materia'>
					<center><input type='submit' class='btn btn-success' value='Confirmar'>
					<a class='btn btn-danger' role='button' href='../index.php'>Cancelar</a>
					<input type='hidden' name='targets' value='$targetsJSON'>"

					?>
				</form>	
			</div>
		</div>
	</body>
	</html>