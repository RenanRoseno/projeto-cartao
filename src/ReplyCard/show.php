<?php
namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');

$id_gabarito = base64_decode($_GET['id_gabarito']);

$query  = "SELECT data,tipo,questoes 
FROM public.gabarito_questoes, gabarito, tipo_gabarito 
WHERE id_gabarito = $id_gabarito AND 
gabarito.id = id_gabarito AND id_tipo = tipo_gabarito.id";

$resul = pg_query($connection, $query);
$result = pg_fetch_array($resul);

$gabarito = json_decode($result['questoes']);

?>
<html>
<head>
	<title> Gabaritos</title>
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<center>
		<div class="container">
			<h4>Cartão Resposta</h4>
			<h6><?php echo $result['tipo'];?></h6>
			<h6><?php echo substr($result['data'], 8, -15)."/".substr($result['data'], 5, -18)."/".substr($result['data'], 0, 4);?></h6>
			<div class="row col-md-2">
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th scope="col">Questão</th>
							<th scope="col">Item</th>
						</tr>
					</thead>
					<tbody>
						<?php

						foreach ($gabarito->targets as $question) {
							echo "<tr>";
							echo "<td>".$question->id."</td>";
							echo  "<td>".$question->item."</td>";
							echo "</tr>";
						}

						?>
					</tbody>
				</table>
			</div>
		</div>
	</body>
	</html>
