<?php
namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');

$id_res = $_GET['id_A'];


$query = "SELECT id, id_aluno, nota, acertos, erros, id_gabarito, respostas, data, img, imgpath
FROM public.gabarito_alunos WHERE id = $id_res";

$result = pg_query($connection, $query);
$result1 = pg_fetch_array($result);

$res1 = pg_query($connection, $query);
$res = pg_fetch_array($res1);

$gabarito = json_decode($res['respostas']);
$id_g = $result1['id_gabarito'];


$query1 = "SELECT * FROM gabarito_questoes WHERE id_gabarito = $id_g;";
$r = pg_query($connection, $query1);
$row = pg_fetch_array($r);
$respost = json_decode($row['questoes']);

$id_aluno = $result1['id_aluno'];
$q = "SELECT * FROM g_alunos WHERE id = $id_aluno";
$re = pg_query($connection, $q);
$res = pg_fetch_array($re);

?>

<html>
<head>
	<title> Gabaritos</title>
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="../../frameworks/open-iconic/font/css/open-iconic.css">
</head>
<body>
	<center>
		<div class="container col-lg-12">
			<h4>Resultados</h4>
			<h6>ID-Aluno: <?php echo $result1['id_aluno'];?></h6>
			<h6>Nome: <?php echo $res['nome']; ?></h6>
			<h6>Data: <?php echo substr($result1['data'], 8, -15)."/".substr($result1['data'], 5, -18)."/".substr($result1['data'], 0, 4);?></h6>
			

			<?php 

			$path = $result1['imgpath'];
			$imageData = file_put_contents("../".$path, pg_unescape_bytea($result1['img'])); 
			//header("content-type: image/jpeg");?>
			<div class="row col-md-2">

				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th scope="col">Questão</th>
							<th scope="col">Item</th>
							<th scope="col">Status</th>
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($respost->targets as $resposta) {
							foreach ($gabarito->targets as $question) {
								if($resposta->id == $question->id){
									$status = ($question->item == $resposta->item) ? "check.svg" : "x.svg";
									echo "<tr>";
									echo "<td>".$question->id."</td>";
									echo  "<td>".$question->item."</td>";
									echo "<td>";
									echo "<center><img src='../../frameworks/open-iconic/svg/$status' height='20' width='20'>";
									echo "</td>";
									echo "</tr>";
								}
							}
						}

						
						echo "<tr>";
						echo "<td colspan=3>";
						$path = base64_encode($path);
						echo '<center><a class="btn btn-primary" role="button" href="showImage.php?path='.$path.'">Imagem</a><br>';			
						echo "</td>";
						echo "</tr>";
						?>

					</tbody>
				</table>
			</div>
			<?php 
			
			?>
		</div>
	</center>
</body>
</html>
