<?php
namespace JansenFelipe\OMR\ReplyCard;
session_start();
?>

<!DOCTYPE html>
<html>
<head>
	<title> Gabaritos</title>
	<link rel="stylesheet" type="text/css" href="../../frameworks/bootstrap/css/bootstrap.min.css">
</head>
<body>
	<div class="header">
		<br><h1 align="center">Criar Cartão-Resposta</h1>
	</div>
	<div class="container">
		<div class="row">
			<div class="col align-items-center offset-md-1 ">
				<form method="POST" action="#" id='form2'>
					<div class="form-group form-inline">
						<label class="label">Matéria / Tipo &nbsp;</label>
						<select class="form-control form-control-sm col-md-2" name="materia">
							<option value="1">CLM- Seleção</option>
						</select>
						&nbsp;&nbsp;	

						<label class="label">Quantidade de questões</label>&nbsp;
						<input type="text" class="form-control col-md-1" name="qtdQuestoes">
						&nbsp;&nbsp;

						<label class="label">Itens &nbsp;</label>
						<select class="form-control form-control-sm col-md-2" name="itens">
							<option value="4">A-D</option>
							<option value="5">A-E</option>
						</select>

						&nbsp;&nbsp;
						<input type="submit" class="btn btn-primary" value="Gerar">
					</div>
				</form>


				<?php
				$erro = 10;

				foreach ( $_POST as $campo => $valor ){
					$campo = trim( strip_tags( $valor ) );
					if ( empty ( $valor ) ) {
						$erro = 1;
						echo 'Existem campos em branco.';
						break;
					}else{
						$erro = 0;
					}
				}

				if($erro == 0){
					$qtdQuestoes = $_POST['qtdQuestoes'];
					$materia = $_POST['materia'];
					$item = $_POST['itens'];
					$itens = ($item == 4) ? array( 0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D') : array( 0 => 'A', 1 => 'B', 2 => 'C', 3 => 'D', 4 => 'E') ;

					$materia = $_POST['materia'];
					
					echo "<div class = 'row offset-md-3 offset-sm-3 mh-10'>
					<form method='POST' action='gabarito.php'  target='_blank'>";

					for($i = 0; $i < $qtdQuestoes; $i++){
						$questao = ($i+1 < 10) ? 0 . ($i+1) : $i + 1;
						echo "Questão ".$questao.": &nbsp;";
						echo "<div class='form-check form-check-inline'>";
						for($j = 0; $j < $_POST['itens'];$j++){
							echo "<input type='radio' class='form-ckeck-input' name='$questao' id='$itens[$j]' value='$itens[$j]' required>&nbsp;";
							echo "<label class='form-check-label' for='$itens[$j]'>$itens[$j]</label>&nbsp;";
						}

						echo "</div><br>";
					}
					echo "<input type='hidden' name='itens' value='$item'>";
					echo "<input type='hidden' name='qtd' value='$qtdQuestoes'>";
					echo "<input type='hidden' name='materia' value='$materia'>";
					echo "<input type='submit' class='btn btn-primary center offset-sm-5' value='Enviar'>";
					echo "</form>";
				}

				?>
			</div>
		</div>
	</div>
</body>