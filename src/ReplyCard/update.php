<?php

namespace JansenFelipe\OMR\ReplyCard;
require('../BD/connection.php');

$id_gabarito = $_POST['id_gabarito'];
$qtd = $_POST['qtd_questoes'];
$elements = [];

for ($i=1; $i <= $qtd ; $i++) { 
	$questao = ($i < 10) ? "0".$i : $i;
	$item = $_POST[$questao];

	$temp = array('id'=> $questao , 'item' => $item);

	array_push($elements, $temp);
}
$targets = array('targets' => $elements);

$targetsJSON = json_encode($targets);

$query = "UPDATE gabarito_questoes SET questoes='$targetsJSON' WHERE id_gabarito = $id_gabarito";

$result = pg_query($connection, $query);

if(!($result))
	echo "erro ao inserir";
else
	echo ("<script LANGUAGE='JavaScript'>
    window.alert('Atualizado com sucesso!');
    window.location.href='../index.php';
    </script>");

?>