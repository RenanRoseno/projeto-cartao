<?php

namespace JansenFelipe\OMR\Maps;

class File {

//----------- DEFAULT ATRIBUTES ----------------
	
	// Config

	private $dpi = 300;

	private $width = 2235;
	private $height = 1640;

	private $topRightX = 2144;
	private $topRightY = 0;

	//---- LIMITS --------

	private $bottomLeftX = 0;
	private $bottomLeftY = 1600;

	// Prova config 	
	private $lastQSeparation = 10;
	private $qtdQuestions = 20;
	private $qtdItens;
	private $id;

	//----- TARGETS
	private $targets;
	private $x1 = 220;
	private $y1 = 620;

	private $x2 = 280;
	private $y2 = 665;

	// Intens config
	private $boxHeight = 60;
	private $boxWidth = 60;

	private $topSpacing = 30;	
	private $spacing = 30;

	// json config
	private $type = "rectangle";

	private $tolerance = 99;
	private $config;


// ------------------------------------------

	
	private function makeItens($qtdItens)
	{
		
		if($qtdItens == 4)
		{
			return array('1' => 'A','2' => 'B','3' => 'C','4' => 'D');
		}
		elseif ($qtdItens == 5) {
			return array('1' => 'A','2' => 'B','3' => 'C','4' => 'D', '5' => 'D');		
		}
	}


	private function config()
	{

		$limits  = array( 
			'topRight' => array( 'x' => $this->topRightX , 'y' => $this->topRightY),
			'bottomLeft' => array( 'x' => $this->bottomLeftX , 'y' => $this->bottomLeftY)
		);

		$config = array(
			'dpi' => $this->dpi, 
			'width' => $this->width,
			'height' => $this->height,
			'limits' => $limits,
			'targets' => 0
		);

		return $config;

	}

// ----------------- setters e getters

// QTD_QUESTIONS
	public function setQtdQuestions($qtdQuestions){
		$this->qtdQuestions = $qtdQuestions;
	}

	public function getQtdQuestions(){
		return $this->qtdQuestions;
	}

// BOX HEIGHT AND WIDTH
	
	public function getHeightBox(){
		return $this->boxHeight;
	}

	public function getWidthBox(){
		return $this->boxWidth;
	}

	public function setBox($width,  $height)
	{
		$this->boxHeight = $height;
		$this->boxWidth =  $width;
	}

// TOP SPACING AND SPACING
	public function setSpacing($topSpacing, $spacing)
	{
		$this->topSpacing = $topSpacing;
		$this->spacing = $spacing;
	}
	
	public function getTopSpacing(){
		return $this->topSpacing;
	}

	public function getScpacing(){
		return $this->spacing;
	}

// LAST Q SEPARATION

	public function setLastQSeparation($lastQSeparation)
	{
		$this->lastQSeparation = $lastQSeparation;
	}	
	
	public function getLastQSeparation(){
		return $this->lastQSeparation;
	}

// X1Y1, X2Y2

	public function setX2Y2($x2,$y2)
	{
		$this->x2 = $x2;
		$this->y2 = $y2;	
	}

	public function setX1Y1($x1,$y1)
	{
		$this->x1 = $x1;
		$this->y1 = $y1;	
	}

	public function getX1(){
		return $this->x1;
	}
	public function getX2(){
		return $this->x2;
	}
	public function getY1(){
		return $this->y1;
	}
	public function getY2(){
		return $this->y2;
	}

// TOlerance
	public function setTolerance($tolerance){
		$this->tolerance = $tolerance;
	}

	public function getTolerance(){
		return $this->tolerance;
	}
// TYPE
	public function setType($type){
		$this->type = $type;
	}

	public function getType(){
		return $this->type;
	}
// BOTTOM CORDINATES

	public function setBottomLeft($x, $y){
		$this->bottomLeftX = $x;
		$this->bottomLeftY = $y;
	}
	public function getBottomLeftX(){ 
		return $this->bottomLeftX;
	}
	public function getBottomLeftY(){ 
		return $this->bottomLeftY;
	}

// TOP RIGHT

	public function setTopRight($x, $y){
		$this->topRightX = $x;
		$this->topRightY = $y;
	}

	public function getTopRightX(){ 
		return $this->topRightX;
	}
	public function getTopRightY(){
		return $this->topRightY;
	}
// WIDTH AND HEIGHT
	public function setDimensions($width, $height){
		$this->width = $width;
		$this->height = $height;
	}

	public function getWidth(){
		return $this->width;
	}

	public function getHeight(){
		return $this->height;
	}

// DPI
	public function setDPI($dpi){
		$this->dpi = $dpi;
	}

	public function getDPI(){
		return $this->dpi;
	}
	
//---------------------------------------------

// --------- SECONDARY FUNCTION

	public function createMap(){

		$file = $this->config();
		$itens = $this->makeItens(4);
		$questions = [];
		
		// Questão 1 - 4 itens
		$x1 = $this->x1 ;
		$y1 = $this->y1 ;

		$x2 = $this->x2 ;
		$y2 = $this->y2 ;
		
		$qtd = $this->qtdQuestions;
		$qtdItens = count($itens); 
		$count = 1;
		$item = 1;

		$topspacing = 0;
		$boxHeight = 0;

		while($qtd > 0){
			while($qtdItens > 0){
				$y1 = $y1 + $topspacing + $boxHeight;
				
				if($topspacing != 0){
					$y2 = $y2 + $topspacing + $boxHeight - 3;
				}else{
					$y2 = $y2 + $topspacing + $boxHeight;
				}

				if($item == 4){
					$y2 += 24;
				}

				$question = array(
					"x1"=> $x1,
					"y1"=> $y1,
					"x2"=> $x2,
					"y2"=> $y2,
					"id"=> ($count < 10) ? '0'.$count.'-'.$itens[$item] : $count.'-'.$itens[$item] ,
					"type"=> $this->type,
					"tolerance"=> $this->tolerance
				);

				array_push($questions, $question);

				$topspacing = $this->topSpacing;
				$boxHeight = $this->boxHeight;
				
				$item++;
				$qtdItens--;
			}

			$y1 = $this->y1 ;
			$x1 = $x1 - $this->spacing - $this->boxWidth;

			$y2 = $this->y2 ;
			$x2 = $x2 - $this->spacing - $this->boxWidth;

			$topspacing = 0;
			$boxHeight = 0;

			$qtdItens = count($itens); 
			$item = 1;
			$count++;

			if($count == $this->lastQSeparation + 1){
				
				//$this->x1 = 1350
				$this->y1 = 909; 
				//$this->x2 = 1388;
				$this->y2 = 968;

				$x1 = $this->x1;
				$x2 = $this->x2;

				$y1 = $this->y1;
				$y2 = $this->y2;

			}

			$qtd--;
		}

		$file['targets'] = $questions;
		
		$file_json = json_encode($file);
		file_put_contents('./Files/map.json', $file_json);
		
		return $file_json;
	}

// ----------- MAIN FUNCTION 

	public function create() 
	{

		$file = $this->config();
		$itens = $this->makeItens(4);
		$questions = [];

		// Questão 1 - 4 itens
		$x1 = $this->x1 ;
		$y1 = $this->y1 ;

		$x2 = $this->x2 ;
		$y2 = $this->y2 ;

		$qtd = $this->qtdQuestions;
		$qtdItens = count($itens); 
		$count = 1;
		$item = 1;

		$spacing = 0;
		$boxWidth = 0;

		while($qtd > 0){
			while($qtdItens > 0){

				$x1 = $x1 + $spacing + $boxWidth;
				$x2 = $x2 + $spacing + $boxWidth;
				
				$question = array(
					"x1"=> $x1,
					"y1"=> $y1,
					"x2"=> $x2,
					"y2"=> $y2,
					"id"=> ($count < 10) ? '0'.$count.'-'.$itens[$item] : $count.'-'.$itens[$item] ,
					"type"=> $this->type,
					"tolerance"=> $this->tolerance
				);

				array_push($questions, $question);

				$spacing = $this->spacing;
				$boxWidth = $this->boxWidth;
				$item++;
				$qtdItens--;

			}

			$x1 = $this->x1 ;
			$y1 = $y1 + $this->topSpacing + $this->boxHeight;

			$x2 = $this->x2 ;
			$y2 = $y2 + $this->topSpacing + $this->boxHeight;

			$spacing = 0;
			$boxWidth = 0;

			$qtdItens = count($itens); 
			$item = 1;
			$count++;
			
			if($count == $this->lastQSeparation + 1){
				
				$this->x1 = 765;
				$this->y1 = 620; 

				$this->x2 = 825;
				$this->y2 = 665;
	
				$x1 = $this->x1;
				$x2 = $this->x2;
				$y1 = $this->y1;
				$y2 = $this->y2;

				$this->tolerance = 98.5;
			}

			$qtd--;
		}

		$file['targets'] = $questions;
		
		$file_json = json_encode($file);
		file_put_contents('./Files/map.json', $file_json);
		
		return $file_json;

	}


	public function createItens(){
		$file = $this->config();
		$itens = $this->makeItens(4);
		$questions = [];

		// Questão 1 - 4 itens
		$x1 = $this->x1 ;
		$y1 = $this->y1 ;

		$x2 = $this->x2 ;
		$y2 = $this->y2 ;

		$qtd = 10;
		$qtdItens = count($itens); 
		$count = 1;
		$item = 1;

		$spacing = 0;
		$boxWidth = 0;
		while($qtd > 0){

			$question = array(
				"x1"=> $x1,
				"y1"=> $y1,
				"x2"=> $x2,
				"y2"=> $y2,
				"id"=> ($count < 10) ? '0'.$count.'-A' : $count.'-A' ,
				"type"=> $this->type,
				"tolerance"=> $this->tolerance
			);

			array_push($questions, $question);

			$spacing = $this->spacing;
			$boxWidth = $this->boxWidth;

			$y2 = $y2 + $this->topSpacing + $this->boxHeight;
			$y1 = $y1 + $this->topSpacing + $this->boxHeight;
			$count++;
			$qtd--;
		}

		$file['targets'] = $questions;
		
		$file_json = json_encode($file);
		file_put_contents('./Files/map.json', $file_json);
		
		return $file_json;
	}
}

?>
