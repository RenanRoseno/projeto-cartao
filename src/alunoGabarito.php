<?php 

namespace JansenFelipe\OMR;

require_once(__DIR__.'/../vendor/autoload.php');

require('Scanners/ImagickScanner.php');
require('Maps/MapJson.php');
require('BD/connection.php');


header('Content-Type: text/html; charset=utf-8');

$id_gab = base64_decode($_GET['id_gab']);
$id_aluno = base64_decode($_GET['id']);
$elements = array('id', 'item' );
$elements = [];



$query = "SELECT * FROM gabarito_alunos WHERE id_aluno = $id_aluno AND id_gabarito = $id_gab";
$result = pg_query($connection, $query);
$qtd_Linhas = pg_num_rows($result);


$q = "SELECT * FROM g_alunos WHERE id = $id_aluno";
$re = pg_query($connection, $q);
$res = pg_fetch_array($re);

if($qtd_Linhas == 0){

  ?>
  <!DOCTYPE html>
  <html>
  <head>
   <title>Gabaritos</title>
   <link rel="stylesheet" type="text/css" href="../frameworks/bootstrap/css/bootstrap.min.css">

 </head>
 <body>
  <center><h2>Cadastrar Gabarito </h2>  
    <h6> Aluno ID: <?php echo $res['id']; ?></h6>
    <h6>Nome: <?php echo $res['nome']; ?></h6>
    <h6>Turma: <?php echo $res['turma']; ?></h6>
    <?php 

    if(!isset($_FILES['imagem'])) {

      ?>
      <br>
      <br>

      <form action="#" method="POST" enctype="multipart/form-data">
       <input class="file" type="file" name="imagem"><br>
       <input type="submit" class="btn btn-success" value="Enviar">
     </form>

     <?php 
   }

   ?>
   <?php

   if(isset($_FILES['imagem']))
   {
      date_default_timezone_set("Brazil/East"); //Definindo timezone padrão


      $ext = strtolower(substr($_FILES['imagem']['name'],-5));
      $name = $id_aluno.'A'.$id_gab.'G'.date("d_m_Y").$ext; 
      $dir = 'Files/'; //Diretório para uploads

      $imagedata = pg_escape_bytea(file_get_contents($_FILES['imagem']['tmp_name']));

      move_uploaded_file($_FILES['imagem']['tmp_name'], $dir.$name);
      echo $finalName = $dir.$name;
    }



//------------- DEBUG ---------------

  //ini_set('display_errors', 1);
  //ini_set('display_startup_errors', 1);

//----------------------------------//

  $gabarito = new Maps\File; // ECHO para debug
  

  $gabarito->create();

  $scanner = new Scanners\ImagickScanner;

  @$imagePath = $dir.$name;
  $mapJsonPath = 'Files/map.json';

  $scanner->setImagePath($imagePath);
  $map = Maps\MapJson::create($mapJsonPath);
  $result = $scanner->scan($map);
  $result = $result->toJson();


  // Correção do cartão resposta
  $acertos = 0;
  $erros = 0;
  $prova = json_decode($result);

  $query = "SELECT questoes, qtd_questoes FROM gabarito_questoes, gabarito WHERE id_gabarito = $id_gab AND gabarito.id = id_gabarito";
  $result = pg_query($connection, $query);
  $row = pg_fetch_array($result);

  $gab = json_decode($row['questoes']);

  foreach ($gab->targets as $key => $resposta) {

    $question = $resposta->id.'-'.$resposta->item;
    
    foreach($prova->targets as $questao)
    {
      if($questao->marked == "yes"){

       if (substr($questao->id, 0, 2) == $resposta->id)
       {
        $status = ($questao->id == $question ) ? "Certa" : "Errada";
        ($status == "Certa") ? $acertos++ : $erros++; 

        $temp = array('id' => substr($questao->id, 0, 2), 'item' => substr($questao->id, -1));

        array_push($elements, $temp);

      }
    }
  }
}


$targets = array('targets' => $elements);

$targets = json_encode($targets);

$total = $row['qtd_questoes']; 
$pont = 10/$total; 

$nota = number_format($pont * $acertos, 2, '.','');

?>

<?php

$data = date("c",time());
$query = "INSERT INTO gabarito_Alunos values (DEFAULT, $id_aluno, $nota, $acertos, $erros, $id_gab, '$targets', '$data','$imagedata', '$finalName');";
//$result = pg_query($connection, $query);


if(!($result))
  echo "erro ao inserir";
else
  echo ("<script LANGUAGE='JavaScript'>
    window.alert('cadastrado com sucesso!');
    window.location.href='index.php';
    </script>");
} else{
  echo ("<script LANGUAGE='JavaScript'>
    window.alert('Resposta deste aluno já cadastrada!');
    window.location.href='index.php';
    </script>");
}
?>

</body>
</html>

