<?php 

namespace JansenFelipe\OMR;

require_once(__DIR__.'/../vendor/autoload.php');

require('Scanners/ImagickScanner.php');
require('Maps/MapJson.php');
require('BD/connection.php');
  $gabarito = new Maps\File; // ECHO para debug
  

  $gabarito->create();
  ?>

  <!DOCTYPE html>
  <html>
  <head>
   <title>Gabaritos</title>
   <link rel="stylesheet" type="text/css" href="../frameworks/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" type="text/css" href="../frameworks/bootstrap/css/style.css">
   <link rel="stylesheet"  type="text/css" href="../frameworks/iziToast/css/iziToast.min.css">
   <script src="../frameworks/iziToast/js/iziToast.min.js" type="text/javascript"></script>

   <script type="text/javascript">
    window.onload =  () => {

      document.getElementById('conteudo').style.display = "none";
      setTimeout( () => {
        document.getElementById('loading').style.display = "none";
        document.getElementById('conteudo').style.display = "block";
      }, 2000);
    }

  </script>
  
</head>
<body>
  <div id="loading" class="center">
    <img src="../images/loading.gif">
  </div>
  <div id="conteudo">
    <br>
    <table align="center">
      <tr>
        <td><a class="btn btn-primary" role="button" href="../qrcode.php">QRcode</a></td>
        <td><a class="btn btn-primary" role="button" href="ReplyCard/ReplyCard.php">Criar</a></td>
      </tr>
    </table>
    <br>
    <center>
      <div class="row col-md-10">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>#</th>
              <th>Tipo</th>
              <th>ID - Professor</th>
              <th>Nome</th>
              <th>Turma</th>
              <th>Data</th>
              <th colspan="4"><center>Ações</th>
              </tr>
            </thead>
            <tbody>
              <?php
              $count = 0;
              $query = "SELECT gabarito.id, nome, turma, id_tipo, professor_id, tipo, data 
              FROM gabarito, tipo_gabarito, gabarito_professor
              WHERE professor_id = 1 AND tipo_gabarito.id = gabarito.id_tipo";

              $result = pg_query($connection, $query);


              while($row = pg_fetch_array($result)){

                $id_gab = base64_encode($row['id']);
                echo "<tr>";
            //echo "<td>".$row['id']."</td>";
                echo "<td>".++$count."</td>";
                echo "<td>".$row['tipo']."</td>"; 
                echo "<td>".$row['professor_id']."</td>";
                echo "<td>".$row['nome']."</td>";
                echo "<td>".$row['turma']."</td>";
                echo "<td>".substr($row['data'], 8, -15)."/".substr($row['data'], 5, -18)."/".substr($row['data'], 0, 4)."</td>";
                echo '<td><center><a class="btn btn-primary btn-sm" role="button" href="ReplyCard/edit.php?id_gabarito='.$id_gab.'">Editar</a></td>';
                echo '<td><center><a class="btn btn-success btn-sm" role="button" href="ReplyCard/results.php?id_gabarito='.$id_gab.'">Resultados</a></td>';
                echo '<td><center><a class="btn btn-info btn-sm" role="button" href="ReplyCard/show.php?id_gabarito='.$id_gab.'">Mostrar</a></td>';
                //echo '<td><center><a class="btn btn-danger btn-sm" role="button" href="ReplyCard/delete.php?id_gabarito='.$id_gab.'">Excluir</a></td>';
                echo '<td><center><button class="btn btn-danger btn-sm" onclick= exibir("'.$id_gab.'")>Excluir</button></td>';
                echo "</tr>"; 
              }


              ?>
              <script type="text/javascript">
                function exibir (id){
                  iziToast.show({
                    theme: 'dark',
                    icon: 'icon-person',
                    message: 'Deseja realmente excluir este gabarito?',
                    position: 'center',
                    progressBarColor: 'rgb(0, 255, 184)',
                    buttons: [
                    ['<button>Confirmar</button>', function (instance, toast) {
                      let url = "ReplyCard/delete.php?id_gabarito=";
                      window.location.href = url.concat(id);
                      
          }], // true to focus
          ['<button>Cancelar</button>', function (instance, toast) {
            instance.hide({
              transitionOut: 'fadeOutUp',
              onClosing: function(instance, toast, closedBy){
                    console.info('closedBy: ' + closedBy); // The return will be: 'closedBy: buttonName'
                  }
                }, toast, 'buttonName');
          }, true]
          ],
          onOpening: function(instance, toast){

            console.info('callback abriu!');
          },
          onClosing: function(instance, toast, closedBy){
        console.info('closedBy: ' + closedBy); // tells if it was closed by 'drag' or 'button'
      }
    });
                }
              </script>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</body>
</html>

