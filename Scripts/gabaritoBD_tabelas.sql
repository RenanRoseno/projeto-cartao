--
-- PostgreSQL database dump
--

-- Dumped from database version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.12 (Ubuntu 10.12-0ubuntu0.18.04.1)

-- Started on 2020-07-17 15:04:23 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 648 (class 1259 OID 20494)
-- Name: gabarito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gabarito (
    id integer NOT NULL,
    id_tipo integer,
    professor_id integer NOT NULL,
    data text NOT NULL,
    qtd_questoes integer NOT NULL,
    qtd_itens integer NOT NULL
);


ALTER TABLE public.gabarito OWNER TO postgres;

--
-- TOC entry 649 (class 1259 OID 20497)
-- Name: gabarito_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gabarito_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gabarito_id_seq OWNER TO postgres;

--
-- TOC entry 4182 (class 0 OID 0)
-- Dependencies: 649
-- Name: gabarito_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gabarito_id_seq OWNED BY public.gabarito.id;


--
-- TOC entry 650 (class 1259 OID 20514)
-- Name: gabarito_questoes; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.gabarito_questoes (
    id integer NOT NULL,
    id_gabarito integer,
    questoes text NOT NULL
);


ALTER TABLE public.gabarito_questoes OWNER TO postgres;

--
-- TOC entry 651 (class 1259 OID 20517)
-- Name: gabarito_questoes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.gabarito_questoes_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.gabarito_questoes_id_seq OWNER TO postgres;

--
-- TOC entry 4183 (class 0 OID 0)
-- Dependencies: 651
-- Name: gabarito_questoes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.gabarito_questoes_id_seq OWNED BY public.gabarito_questoes.id;


--
-- TOC entry 646 (class 1259 OID 20475)
-- Name: tipo_gabarito; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tipo_gabarito (
    id integer NOT NULL,
    tipo text
);


ALTER TABLE public.tipo_gabarito OWNER TO postgres;

--
-- TOC entry 647 (class 1259 OID 20482)
-- Name: tipo_gabarito_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tipo_gabarito_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipo_gabarito_id_seq OWNER TO postgres;

--
-- TOC entry 4184 (class 0 OID 0)
-- Dependencies: 647
-- Name: tipo_gabarito_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tipo_gabarito_id_seq OWNED BY public.tipo_gabarito.id;


--
-- TOC entry 4040 (class 2604 OID 20499)
-- Name: gabarito id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gabarito ALTER COLUMN id SET DEFAULT nextval('public.gabarito_id_seq'::regclass);


--
-- TOC entry 4041 (class 2604 OID 20519)
-- Name: gabarito_questoes id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gabarito_questoes ALTER COLUMN id SET DEFAULT nextval('public.gabarito_questoes_id_seq'::regclass);


--
-- TOC entry 4039 (class 2604 OID 20484)
-- Name: tipo_gabarito id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_gabarito ALTER COLUMN id SET DEFAULT nextval('public.tipo_gabarito_id_seq'::regclass);


--
-- TOC entry 4173 (class 0 OID 20494)
-- Dependencies: 648
-- Data for Name: gabarito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gabarito (id, id_tipo, professor_id, data, qtd_questoes, qtd_itens) FROM stdin;
\.


--
-- TOC entry 4175 (class 0 OID 20514)
-- Dependencies: 650
-- Data for Name: gabarito_questoes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.gabarito_questoes (id, id_gabarito, questoes) FROM stdin;
\.


--
-- TOC entry 4171 (class 0 OID 20475)
-- Dependencies: 646
-- Data for Name: tipo_gabarito; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tipo_gabarito (id, tipo) FROM stdin;
1	CLM - Seleção
\.


--
-- TOC entry 4185 (class 0 OID 0)
-- Dependencies: 649
-- Name: gabarito_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gabarito_id_seq', 84, true);


--
-- TOC entry 4186 (class 0 OID 0)
-- Dependencies: 651
-- Name: gabarito_questoes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.gabarito_questoes_id_seq', 40, true);


--
-- TOC entry 4187 (class 0 OID 0)
-- Dependencies: 647
-- Name: tipo_gabarito_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tipo_gabarito_id_seq', 1, true);


--
-- TOC entry 4045 (class 2606 OID 20501)
-- Name: gabarito gabarito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gabarito
    ADD CONSTRAINT gabarito_pkey PRIMARY KEY (id);


--
-- TOC entry 4047 (class 2606 OID 20521)
-- Name: gabarito_questoes gabarito_questoes_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gabarito_questoes
    ADD CONSTRAINT gabarito_questoes_pkey PRIMARY KEY (id);


--
-- TOC entry 4043 (class 2606 OID 20486)
-- Name: tipo_gabarito tipo_gabarito_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tipo_gabarito
    ADD CONSTRAINT tipo_gabarito_pkey PRIMARY KEY (id);


--
-- TOC entry 4048 (class 2606 OID 20506)
-- Name: gabarito gabarito_id_tipo_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gabarito
    ADD CONSTRAINT gabarito_id_tipo_fkey FOREIGN KEY (id_tipo) REFERENCES public.tipo_gabarito(id);


--
-- TOC entry 4049 (class 2606 OID 20527)
-- Name: gabarito_questoes gabarito_questoes_id_gabarito_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.gabarito_questoes
    ADD CONSTRAINT gabarito_questoes_id_gabarito_fkey FOREIGN KEY (id_gabarito) REFERENCES public.gabarito(id);


-- Completed on 2020-07-17 15:04:23 -03

--
-- PostgreSQL database dump complete
--

