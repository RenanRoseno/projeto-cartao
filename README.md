# Projeto-cartão (Leitor de gabaritos)

> Atualmente o projeto encontra-se em sua primeira versão.

## Informações do projeto

### Arquivo index.php encontra-se em:

> projeto-cartao/src

### Bibliotecas utilizadas na aplicação:
- JansenFelipe/OMR - Optical Mark Recognition;
- chillerlan/php-qrcode.

### Requisitos da aplicação
- PHP 7.4+;
- ImageMagick 6; 
- imagick-3.4.4 extension;
- mbstring extension.

### Links das bibliotecas:
- [JansenFelipe/OMR](https://github.com/jansenfelipe/omr) ;
- [chillerlan/php-qrcode](https://github.com/chillerlan/php-qrcode) .